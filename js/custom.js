function canvasLoad() {
  var jqxhr = $.getJSON("https://jsonplaceholder.typicode.com/photos", function(
    d
  ) {
    //console.log( "success" + d );
    datprocess(d);
  })
    .done(function() {
      //console.log( "second success" );
    })
    .fail(function() {
      //console.log( "error" );
    })
    .always(function() {
      //console.log( "complete" );
    });

  jqxhr.complete(function() {
    //console.log( "second complete" );
  });
}

function datprocess(jsondata) {
  //console.log(jsondata.length);
  var dynamicSelect = $("#customSelect");
  dynamicSelect.empty().append('<option value="0">Canvas #</option>');
  var nodeData;

  //console.log(getUniqueInt(2, 4));
  var uId = getUniqueInt(2, 4);
  var canvasCont = $("#canvasCont");
  canvasCont.empty();

  for (var i = 0; i < uId; i++) {
    dynamicSelect.append(
      '<option id="customSelectOption' +
        (i + 1) +
        '" value="' +
        (i + 1) +
        '">Canvas ' +
        (i + 1) +
        "</option>"
    );
    canvasCont.append(
      '<div class="canvasbox"><canvas width="548" height="548" id="canvasbox' +
        (i + 1) +
        '"><span class="canvasLabel">' +
        (i + 1) +
        "</span></canvas></div>"
    );
  }

  var storejson = jsondata;

  $("#canvasInsert").on("click", function() {
    var selectedCanvas = dynamicSelect.val();
    if (selectedCanvas != 0) {
      insertIntoCanvas(storejson, selectedCanvas, uId);
    }
  });

  dynamicSelect.css("display", "inline-block");
  $("#canvasInsert").css("display", "inline-block");
}

function insertIntoCanvas(storejson, canvasNumber, uId) {
  var rNode = getUniqueInt(3, 4997);
  var canvasIdParent = "canvasbox" + canvasNumber;
  var canvasIdChild = new fabric.Canvas(canvasIdParent);
  var posX = canvasIdChild.width * Math.random();
  var posY = canvasIdChild.height * Math.random();
  for (var i = 0; i < storejson.length; i++) {
    //debugger;
    if (
      i == 0 ||
      i == 1 ||
      i == storejson.length - 2 ||
      i == storejson.length - 1 ||
      i == rNode
    ) {
      nodeData = storejson[i];
      //console.log(nodeData);
      var thumbnailUrl = "",
        title = "",
        url = "";

      if (isOdd(nodeData["id"]) == "odd") {
        fabric.Image.fromURL(nodeData["thumbnailUrl"], function(myImg) {
          //i create an extra var for to change some image properties
          var thumbnailUrl = myImg.set({
            left: 50,
            top: 10,
            width: 150,
            height: 150,
            hasControls: false
          });
          canvasIdChild.add(thumbnailUrl);
        });
      }

      if (isOdd(nodeData["id"]) == "even") {
        title = new fabric.Text(nodeData["title"], {
          fontSize: 30,
          originX: "center",
          originY: "center",
          left: 150,
          top: 150,
          hasControls: false
        });
        canvasIdChild.add(title);
      }

      if (nodeData["albumId"] >= 100) {
        url = new fabric.Text(nodeData["url"], {
          fontSize: 30,
          originX: "center",
          originY: "center",
          left: 200,
          top: 200,
          hasControls: false
        });
        canvasIdChild.add(url);
      }
    }
  }
  setTimeout(function() {
    moveObjects(canvasIdChild, uId, canvasNumber);
  }, 1000);
}

function moveObjects(canvasIdChild, uId, canvasNumber) {
  var canvasId;
  var canvases = {
    canvasbox: canvasIdChild
  };

  for (var i = 1; i <= uId; i++) {
    if (i == canvasNumber) {
      continue;
    }
    canvases["canvasbox" + i] = new fabric.Canvas("canvasbox" + i);
  }

  var activeObject, initialCanvas;
  canvases.canvasbox.on("mouse:down", function() {
    if (this.getActiveObject()) {
      activeObject = $.extend({}, this.getActiveObject());
      initialCanvas = this.lowerCanvasEl.id;
    }
  });

  $(document).on("mouseup", function(evt) {
    //console.log($(evt.target));
    if (evt.target.localName === "canvas" && initialCanvas) {
      canvasId = $(evt.target)
        .prev()
        .attr("id");
      if (canvasId !== initialCanvas) {
        canvases[canvasId].add(activeObject);
        canvases[canvasId].renderAll();
      }
    }
    initialCanvas = "";
    activeObject = {};
  });
}

function getUniqueInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isOdd(num) {
  //return num % 2;
  if (num % 2 == 0) {
    return "even";
  } else {
    return "odd";
  }
}

$(function() {
  if ($("#canvas").length > 0) {
    //canvas();
    $("#canvasGenrate").on("click", function() {
      canvasLoad();
    });
  }
});
